import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.Test;
import pages.*;
import util.LocalDriverManager;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

public class SearchTests extends BaseTests {

    //*********Tests*********
    @Test(invocationCount = 1, groups = {"smoke", "functional"}, description = "Run a product search via search bar, add filters" +
            " and validate results")
    public void testSearchViaSearchbar() {
        WebDriver driver = LocalDriverManager.getDriver();
        HomePage homePage = new HomePage(driver);
        Popups popups = new Popups(driver);
        SearchPage searchPage = new SearchPage(driver);
        String searchQuery = "jeans";
        String [] updatedColourFilters= {"black","blue"};
        String updatedDivisionFilter= "Women";

        homePage.goToHomePageAndDismissPopups(popups);
        searchPage.searchViaSearchBar(searchQuery);
        searchPage.verifySearchResults(100, 12, searchQuery);

        searchPage.verifyUpdateFilters(updatedColourFilters, updatedDivisionFilter, 20);
        searchPage.verifySearchResults(12, 12,  searchQuery);
    }

    @Test(invocationCount = 1 , groups = {"smoke", "functional"}, description = "Add items to bag from search" +
            " and validate their presence in the shopping bag")
    public void testAddItemsToBag() throws ParseException {
        WebDriver driver = LocalDriverManager.getDriver();
        HomePage homePage = new HomePage(driver);
        Popups popups = new Popups(driver);
        ProductDisplayPage productDisplayPage = new ProductDisplayPage(driver);
        SearchPage searchPage = new SearchPage(driver);
        ShoppingBagPage shoppingBagPage = new ShoppingBagPage(driver);
        String [] searchQueries = {"/gap/men/clothing/t-shirts/" , "/gap/men/clothing/polos/"};
        ITestContext ctx = Reporter.getCurrentTestResult().getTestContext();

        homePage.goToHomePageAndDismissPopups(popups);
        List<HashMap> productsAddedToBagFromSearch = searchPage.searchForProductsAndAddToBag(searchQueries, productDisplayPage);
        ctx.setAttribute("productsAddedToBagFromSearch",productsAddedToBagFromSearch);
        shoppingBagPage.goToShoppingBagPage();
        shoppingBagPage.verifyProductCountInBag(productsAddedToBagFromSearch);
        shoppingBagPage.verifyProductsInBagMatchAddedProducts(productsAddedToBagFromSearch);
    }

    @Test(invocationCount = 1 , groups = {"functional"}, description = "Add items to bag" +
            " remove them and validate that both shopping bag and mini shopping bag are empty")
    public void testRemoveAllItemsFromBag() throws ParseException {
        WebDriver driver = LocalDriverManager.getDriver();
        HomePage homePage = new HomePage(driver);
        MiniShoppingBagPage miniShoppingBagPage = new MiniShoppingBagPage(driver);
        Popups popups = new Popups(driver);
        ProductDisplayPage productDisplayPage = new ProductDisplayPage(driver);
        SearchPage searchPage = new SearchPage(driver);
        ShoppingBagPage shoppingBagPage = new ShoppingBagPage(driver);
        String [] searchQueries = {"/gap/men/clothing/t-shirts/" , "/gap/men/clothing/polos/"};

        homePage.goToHomePageAndDismissPopups(popups);
        List<HashMap> productsAddedToBagFromSearch = searchPage.searchForProductsAndAddToBag(searchQueries, productDisplayPage);
        shoppingBagPage.goToShoppingBagPage();
        shoppingBagPage.verifyProductCountInBag(productsAddedToBagFromSearch);
        shoppingBagPage.verifyRemoveAllItemsFromBag();
        miniShoppingBagPage.verifyMiniShoppingBagIsEmpty();
    }

    @Test(invocationCount = 1, priority = 2, groups = {"functional"}, description = "Adding an item to to shopping bag" +
            " remove them also adds it to miniBag")
    public void testAddingProductIsReflectedInMiniBag() throws ParseException {
        WebDriver driver = LocalDriverManager.getDriver();
        HomePage homePage = new HomePage(driver);
        MiniShoppingBagPage miniShoppingBagPage = new MiniShoppingBagPage(driver);
        Popups popups = new Popups(driver);
        ProductDisplayPage productDisplayPage = new ProductDisplayPage(driver);
        SearchPage searchPage = new SearchPage(driver);
        String [] searchQueries = {"/gap/men/clothing/t-shirts/"};

        homePage.goToHomePageAndDismissPopups(popups);
        List<HashMap> productsAddedToBagFromSearch = searchPage.searchForProductsAndAddToBag(searchQueries, productDisplayPage);
        miniShoppingBagPage.verifyMiniBagIconItemCount(productsAddedToBagFromSearch);

        miniShoppingBagPage.mouseOverMiniBag();
        miniShoppingBagPage.verifyProductsInBagMatchAddedProducts(productsAddedToBagFromSearch);
    }

    //*********Helper Methods*********

}
