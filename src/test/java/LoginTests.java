
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.*;
import pages.checkout.RegistrationPage;
import util.LocalDriverManager;
import util.StringUtilities;

import java.io.IOException;

public class LoginTests extends BaseTests {

    //*********Tests*********
    @Test(groups = {"functional"}, description = "User navigates to Sign in page, submits an invalid username" +
            " and password. Expected Result: Receive Error Text")
    public void invalidLoginTest() {
        WebDriver driver = LocalDriverManager.getDriver();
        HomePage homePage = new HomePage(driver);
        LoginPage loginPage = new LoginPage(driver);
        Popups popups = new Popups(driver);
        TopNav topNav = new TopNav(driver);
        String invalidUserName = "tester123@tester.com";
        String invalidPassword = "invalidPassword123";
        String invalidLoginMessage = "We didn't recognize the username or password you entered. Please try again.";

        homePage.goToHomepage();
        popups.dismissCookiesAgreementPolicy();
        loginPage.loginUser(invalidUserName, invalidPassword, topNav);
        loginPage.verifyLoginErrorText(invalidLoginMessage);
    }

    @Test(dependsOnMethods ={"testRegisterUser"} ,groups = {"smoke", "functional", "checkout"},
            description = "User navigates to Sign in page, submits a previously created username and password")
    public void testValidLogin() {
        WebDriver driver = LocalDriverManager.getDriver();
        AccountPage acctPage = new AccountPage(driver);
        HomePage homePage = new HomePage(driver);
        LoginPage loginPage = new LoginPage(driver);
        Popups popups = new Popups(driver);
        TopNav topNav = new TopNav(driver);
        ITestContext ctx = Reporter.getCurrentTestResult().getTestContext();
        String userName = (String)ctx.getAttribute("email");
        String password = (String)ctx.getAttribute("password");
        String expectedWelcomeMessage = (String)ctx.getAttribute("welcomeMessage");

        homePage.goToHomepage();
        popups.dismissCookiesAgreementPolicy();
        loginPage.loginUser(userName, password, topNav);
        acctPage.assertSuccesfulLogin(expectedWelcomeMessage);
    }

    @DataProvider(name="getTestRegisterUser")
    public Object[][] getTestRegisterUserDataProvider() throws IOException {
        return getDataProvider("testRegisterUser");
    }

    @Test(groups = {"functional", "checkout"}, dataProvider = "getTestRegisterUser",description = "User navigates to Register page " +
            "and submits Registration info" )
    public void testRegisterUser(String description, String email, String password, String firstName, String surName,
                                 String postCode, String welcomeMessage) {
        WebDriver driver = LocalDriverManager.getDriver();
        HomePage homePage = new HomePage(driver);
        LoginPage loginPage = new LoginPage(driver);
        RegistrationPage regPage = new RegistrationPage(driver);
        Popups popups = new Popups(driver);
        TopNav topNav = new TopNav(driver);
        //generate unique email
        email = StringUtilities.generateUniqueId() +email;
        ITestContext ctx = Reporter.getCurrentTestResult().getTestContext();
        ctx.setAttribute("email",email);
        ctx.setAttribute("password", password);
        ctx.setAttribute("welcomeMessage",welcomeMessage);

        homePage.goToHomepage();
        popups.dismissCookiesAgreementPolicy();

        regPage.goToRegistrationPage(topNav);
        regPage.registerUser(email, password, firstName, surName, postCode);
        regPage.verifyPageTitleIsLoginPage(loginPage);
    }


    //*********Helper Methods*********

}
