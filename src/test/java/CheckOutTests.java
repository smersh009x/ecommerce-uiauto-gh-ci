import org.openqa.selenium.WebDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.ShoppingBagPage;
import pages.checkout.PaymentDetailsPage;
import pages.checkout.ShippingPage;
import util.LocalDriverManager;

import java.io.IOException;
import java.text.ParseException;

public class CheckOutTests extends BaseTests {

    //*********Tests*********

    @DataProvider(name="getCheckoutProcessLoggedInUser")
    public Object[][] getTestCheckoutProcessLoggedInUserDataProvider() throws IOException {
        return getDataProvider("testCheckoutProcessLoggedInUser");
    }

    @Test(invocationCount = 1, dataProvider = "getCheckoutProcessLoggedInUser",groups = {"functional"},
            description = "Log in. Add an item to to shopping bag check out")
    public void testCheckoutProcessLoggedInUser(String description, String email, String password, String firstName, String surName,
                                                String postCode, String welcomeMessage,String address, String city,
                                                String addressNickname,String phoneNumber, String country)  throws ParseException {
        WebDriver driver = LocalDriverManager.getDriver();
        LoginTests login = new LoginTests();
        PaymentDetailsPage pdp = new PaymentDetailsPage(driver);
        ShippingPage shippingPage = new ShippingPage(driver);
        SearchTests search = new SearchTests();
        ShoppingBagPage shoppingBagPage = new ShoppingBagPage(driver);

        login.testRegisterUser(description, email,  password,  firstName,  surName, postCode,  welcomeMessage);
        login.testValidLogin();

        search.testAddItemsToBag();
        shoppingBagPage.clickCheckout();
        shippingPage.enterShippingInfoAndSubmit(country,firstName,surName, address, city,addressNickname,phoneNumber);
        shippingPage.verifyAddressVerificationDisplayed(address, city);
        shippingPage.clickConfirmAddressOnAddressVerification();
        pdp.verifyEnterPaymentDetailsPageIsShowing();
        //TODO Cannot proceed much further as we don't want to submit an order on production
    }


    //*********Helper Methods*********

}
