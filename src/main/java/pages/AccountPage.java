package pages;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;


public class AccountPage extends BasePage {

    public AccountPage(WebDriver driver) {
        super(driver);
    }

    //*********Web Elements*********
    private By welcomeUserMessage = By.cssSelector("#main .my-account__heading");

    //*********Page Methods*********
    public void assertSuccesfulLogin(String expectedWelcomeUserMsg){
        String actualWelcomeUserMsg = StringUtils.strip(getTextByAttribute(welcomeUserMessage));

        Assert.assertEquals(expectedWelcomeUserMsg, actualWelcomeUserMsg);
    }

}