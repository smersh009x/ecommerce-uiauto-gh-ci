package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import util.StringUtilities;


public class TopNav extends BasePage {

    public TopNav(WebDriver driver) {
        super(driver);
    }

    //*********Web Elements*********
    private By lnkUserAccount = By.cssSelector(".your-account a.user-account");
    private By lnkLogin = By.cssSelector(".user-links a[href='"+BASE_URL+"/account']");
    private By lnkRegister = By.cssSelector(".user-links a.sign-in-link");

    //*********Page Methods*********

    public void openUserAccountMenu(){
        click(lnkUserAccount);
    }

    public void clickSignInUser(){
        click(lnkLogin);
    }

    public void clickRegisterUser(){
        click(lnkRegister);
    }

}