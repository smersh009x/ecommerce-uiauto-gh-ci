package pages;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import util.NumberUtilities;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class ProductDisplayPage extends BasePage {

    public ProductDisplayPage(WebDriver driver) {
        super(driver);
    }

    //*********Web Elements*********
    private By imgSizeSelectable = By.cssSelector("li.size .swatches__size.gap.selectable a");
    private By imgColorSelectable = By.cssSelector("li.color .swatches__color.gap.selectable a");
    private By btnAddToBag = By.cssSelector(".pdpForm button#add-to-cart");
    private By txtProductName = By.cssSelector(".product-detail .pdp_product-name");
    private By txtProductPrice = By.cssSelector(".product-detail .product-price > span > span:not([class *= 'high'])");
    private By txtProductSize = By.cssSelector(".size .selected-value");
    private By txtProductColor = By.cssSelector(".color .selected-value");
    private By btnProductQuantitySelected = By.cssSelector(".quantity__selector .quantity__item--selected");
    private String btnProductQuantitySelector = ".inventory ul.quantity__selector.quantity__selector--active li:nth-child(%s)";

    //*********Page Methods*********
    public HashMap<String, String> selectProductAttributesAndAddToBag() throws ParseException {
        clickOnASelectableSize();
        clickOnSelectableColor();
        clickUpdateQuantity();
        HashMap<String, String> selectedProduct = getSelectedProductAttributesForBagDisplay();
        clickAddItemToBag();
        return selectedProduct;
    }


    public void clickOnASelectableSize() {
        WebElement el = getVisibleElement(imgSizeSelectable);
        click(el);
        waitForStalenessOfElement(el);
    }

    public void clickOnSelectableColor() {
        List<WebElement> availableColors = driver.findElements(imgColorSelectable);
        if (availableColors.size() > 1) {
            WebElement el = getVisibleElement(imgColorSelectable);
            click(el);
            waitForStalenessOfElement(el);
        } else {
            click(imgColorSelectable);
        }
    }

    public int clickUpdateQuantity() {
        String quantityNumber = "2";

        //Only update quantity of product to <quantityNumber> if there is more than quantityNumber+1 available
        // to help prevent occasional item just sold out error
        if (getAvailableQuantity() >= 3) {
            WebElement el = getVisibleElement(btnProductQuantitySelected);
            click(el);
            click(By.cssSelector(String.format(btnProductQuantitySelector, quantityNumber)));
            waitForTextUpdate(btnProductQuantitySelected, quantityNumber);
            return NumberUtilities.extractIntfromString(quantityNumber);
        }
        //Don't update quantity
        return 1;

    }

    private int getAvailableQuantity(){
        WebElement quantity = getPresentElement(btnAddToBag);
        return NumberUtilities.extractIntfromString(quantity.getAttribute("data-available"));
    }

    public void clickAddItemToBag() {
        click(btnAddToBag);
    }

    public HashMap<String, String> getSelectedProductAttributesForBagDisplay() throws ParseException {
        HashMap<String, String> productAttributes = new HashMap<String, String>() {
            {
                put("name", getProductName());
                put("color", getProductColor());
                put("size", getProductSize());
                put("price", getProductPrice());
                put("quantity", getProductQuantity());
            }
        };
        return productAttributes;
    }

    public String getProductName() {
        String productName = StringUtils.strip(getTextByAttribute(txtProductName));
        return productName;
    }

    public String getProductPrice() {
        String productPrice = StringUtils.strip(getTextByAttribute(txtProductPrice).replace("-", ""));
        return productPrice;
    }


    public String getProductSize() {
        String productSize = StringUtils.strip(getTextByAttribute(txtProductSize));
        return productSize;
    }

    public String getProductColor() {
        String productColor = StringUtils.strip(getTextByAttribute(txtProductColor));
        return productColor;
    }

    public String getProductQuantity() {
        String productQuantity = StringUtils.strip(getTextByAttribute(btnProductQuantitySelected));
        return productQuantity;
    }


}