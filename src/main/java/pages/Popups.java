package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import java.util.Set;


public class Popups extends BasePage {



    public Popups(WebDriver driver) {
        super(driver);
    }

    //*********Web Elements*********
    private By btnCookiesAgreement = By.cssSelector("#consent_prompt_submit");
    private By toggleOffersPopup = By.cssSelector(".nav-banner .promo-drawer__trigger-icon");

    //*********Page Methods*********
    public void dismissAllVisiblePopups() {
        dismissCookiesAgreementPolicy();
        showAndDismissOffersPopup();
    }

    public void dismissCookiesAgreementPolicy(){
        //If cookie isn't there, then we dismiss the dialog
        if(driver.manage().getCookieNamed("CONSENTMGR") == null){
                click(btnCookiesAgreement);
        }
    }
    // Gap is currently running a promo that shows up if the screen is scrolled downward
    // This is a hacky workaround, ideally we would use an A/B test variation giving us the option
    // to only show it during certain tests
    public void showAndDismissOffersPopup(){
        if (isElementPresent(toggleOffersPopup)){
            scrollPageDown();
            click(toggleOffersPopup);
            scrollPageUp();
        }
    }

}



