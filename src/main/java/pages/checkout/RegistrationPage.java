package pages.checkout;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pages.BasePage;
import pages.LoginPage;
import pages.TopNav;

public class RegistrationPage extends BasePage {

    private String txtPassword = "#RegistrationForm input[placeholder='Password']";
    private String txtConfirmPassword = "#RegistrationForm input[placeholder='Confirm Password']";
    private String txtFirstName = "#RegistrationForm input[name='dwfrm_profile_customer_firstname']";
    private String txtSurName = "#RegistrationForm input[name='dwfrm_profile_customer_lastname']";
    private String txtPostCode = "#RegistrationForm input[name='dwfrm_profile_customer_postal']";
    private String btnRegister = "#RegistrationForm button[name='dwfrm_profile_confirm']";


    public RegistrationPage(WebDriver driver) {
        super(driver);

    }

    //*********Web Elements*********
    private By txtEmail = By.cssSelector("#RegistrationForm input[name='dwfrm_profile_customer_email']");

    //*********Page Methods*********

    public void goToRegistrationPage(TopNav topNav){
        topNav.openUserAccountMenu();
        topNav.clickRegisterUser();
    }

    public void registerUser(String email, String password, String firstName, String surName, String postCode) {
        String registrationPageTitle = getPageTitle();
        enterEmail(email);
        enterPassword(password);
        reenterPassword(password);
        enterFirstName(firstName);
        enterSurName(surName);
        enterPostCode(postCode);
        clickRegister();
    }

    private void reenterPassword(String password) {
        clearAndTypeText(By.cssSelector(txtConfirmPassword), password);
    }

    public void enterPassword(String password) {
        clearAndTypeText(By.cssSelector(txtPassword), password);
    }

    public void enterFirstName(String firstName) {
        clearAndTypeText(By.cssSelector(txtFirstName), firstName);
    }

    public void enterSurName(String surName) {
        clearAndTypeText(By.cssSelector(txtSurName), surName);
    }

    public void enterPostCode(String postCode) {
        clearAndTypeText(By.cssSelector(txtPostCode), postCode);
    }

    public void clickRegister() {
        click(By.cssSelector(btnRegister));
    }

    public void enterEmail(String email) {
        clearAndTypeText(txtEmail, email);
    }
    //*********Verifications*********
    //Upon succesful registration Gap redirects the user to the login page
    //there is no "registration Success" type of message to verify
    public void verifyPageTitleIsLoginPage(LoginPage loginPage) {
        String currentPageTitle = getPageTitle();
        Assert.assertEquals(currentPageTitle, loginPage.getExpectedPageTitle());
    }
}