package pages.checkout;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pages.BasePage;

import java.util.List;

public class ShippingPage extends BasePage {



    public ShippingPage(WebDriver driver) {
        super(driver);
    }

    //*********Web Elements*********
    private By txtFirstName = By.cssSelector("#dwfrm_singleshipping_shippingAddress input[name='dwfrm_singleshipping_shippingAddress_addressFields_firstName']");
    private By txtSurName = By.cssSelector("#dwfrm_singleshipping_shippingAddress input[name='dwfrm_singleshipping_shippingAddress_addressFields_lastName']");
    private By txtAddress = By.cssSelector("#dwfrm_singleshipping_shippingAddress input[name='dwfrm_singleshipping_shippingAddress_addressFields_address1']");
    private By txtCity = By.cssSelector("#dwfrm_singleshipping_shippingAddress input[name='dwfrm_singleshipping_shippingAddress_addressFields_city']");
    private By txtNickname = By.cssSelector("#dwfrm_singleshipping_shippingAddress input[name='dwfrm_singleshipping_shippingAddress_addressFields_nickname']");
    private By txtPhone = By.cssSelector("#dwfrm_singleshipping_shippingAddress input[name='dwfrm_singleshipping_shippingAddress_addressFields_phone']");
    private By btnShippingSubmit = By.cssSelector(".page-checkout__button #shipping-submit");
    private By selectCountry = By.cssSelector("#dwfrm_singleshipping_shippingAddress select[name='dwfrm_singleshipping_shippingAddress_addressFields_country']");
    private By loaderIndicator = By.cssSelector(".loader-indicator");
    private By txtAddressValidation = By.cssSelector(".address-validation .ui-dialog-title");
    private By btnConfirmAddress = By.cssSelector("button#address-confirm");
    private String ensureAccurateDeliveryTitle = "Ensure Accurate Delivery";

    //*********Page Methods*********
    public void enterFirstName(String firstName) {
        clearAndTypeText(txtFirstName, firstName);
    }

    public void enterSurName(String surName) {
        clearAndTypeText(txtSurName, surName);
    }

    public void enterAddress(String address) {
        clearAndTypeText(txtAddress, address);
    }

    public void enterCity(String city) {
        clearAndTypeText(txtCity, city);
    }

    public void enterAddressNickName(String nickName) {
        clearAndTypeText(txtNickname, nickName);
    }

    public void enterPhone(String phone) {
        clearAndTypeText(txtPhone, phone);
    }

    public void clickShippingSubmit(){
        click(btnShippingSubmit);
    }

    public void selectCountry(String country){
        selectFromDropdown(selectCountry, country);
        waitForInvincibility(loaderIndicator);
    }

    public void enterShippingInfoAndSubmit(String country, String firstName, String surName, String address,
                                           String city, String addressNickname, String phoneNumber) {
        selectCountry(country);
        enterAddress(address);
        enterCity(city);
        enterAddressNickName(addressNickname);
        enterPhone(phoneNumber);
        enterFirstName(firstName);
        enterSurName(surName);
        clickShippingSubmit();
    }

    public void clickConfirmAddressOnAddressVerification(){
        click(btnConfirmAddress);
    }

    //*********Verifications*********
    public void verifyAddressVerificationDisplayed(String address, String city){
        List<WebElement> found = waitForVisibilityOfAllElements(txtAddressValidation);
        String addressDisplayed = getTextByAttribute(By.cssSelector(".original-address .addressline1"));
        String cityDisplayed  = getTextByAttribute(By.cssSelector(".original-address .addressline3 span.city"));

        Assert.assertEquals(found.get(0).getText(),ensureAccurateDeliveryTitle);
        Assert.assertEquals(addressDisplayed, address);
        Assert.assertEquals(cityDisplayed, city);
    }


}