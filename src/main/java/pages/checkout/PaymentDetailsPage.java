package pages.checkout;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pages.BasePage;

import java.util.List;

public class PaymentDetailsPage extends BasePage {

    public PaymentDetailsPage(WebDriver driver) {
        super(driver);
    }

    //*********Web Elements*********
    private By enterCardDetails = By.cssSelector(".payment-method-expanded .page-checkout__title");
    private String txtEnterCardDetails = "Enter Card Details";


    //*********Page Methods*********

    //*********Verifications*********
    public void verifyEnterPaymentDetailsPageIsShowing(){
        List<WebElement> found = waitForVisibilityOfAllElements(enterCardDetails);
        Assert.assertEquals(found.get(0).getText(),txtEnterCardDetails);
    }

}