package pages;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.log4testng.Logger;
import util.NumberUtilities;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class SearchPage extends BasePage {
    private static final Logger LOGGER = Logger.getLogger(SearchPage.class);

    public SearchPage(WebDriver driver) {
        super(driver);
    }

    //*********Web Elements*********
    private By txtQueryInput = By.cssSelector(".search-wrapper input#q");
    private By btnSubmitQuery = By.cssSelector("button[type='submit'] .icon-magnifying-glass-outer_small");
    private By txtResultCount = By.cssSelector("div.refinements.refinement-bar > div");
    private By txtResultQuery = By.cssSelector(".breadcrumb-result-text a[href*='/search?q=']");
    private By initialProductsReturned = By.cssSelector("div.product-image");
    private By lnkDivisionFilter = By.cssSelector("#main .refinement-bar li:nth-child(1) > div > div > div");
    private String ddlSelectDivisionFilter= "//div[@id='main']//div[@class='js-refinement-heading" +
            " refinement-heading department']/ul/li/a[normalize-space(text()) ='%s']";
    private By lnkColourFilter = By.cssSelector("#main .refinement-bar li:nth-child(2) > div > div > div");
    private String lnkSelectColourFilter = "#main li.refinement-bar__filter a#swatch-%s";
    private By lnkPriceFilter = By.cssSelector("#main .refinement-bar li:nth-child(3) > div > div > div");
    private By sldPriceSliderLeft = By.cssSelector("#main .ui-slider-handle[style='left: 0%;']");
    private By txtNewPriceAfterFilterApplied = By.cssSelector("#main .price-amount");
    private String txtFilterApplied = "//*[@id='main']//a[contains(@class, 'refinement-relax')]"
            + "[normalize-space()='%s']";

    //*********Page Methods*********
    public void searchViaSearchBar(String query) {
        clearAndTypeText(txtQueryInput, query);
        click(btnSubmitQuery);
    }

    public void updatePriceFilterAndVerify(int xOffset) {
        click(lnkPriceFilter);
        WebElement slider = getVisibleElement(sldPriceSliderLeft);
        moveSlider(slider, xOffset);
        String newPrice = getTextByAttribute(txtNewPriceAfterFilterApplied).replace("-","to");
        verifyFilterApplied(newPrice);
        click(lnkPriceFilter);
    }

    public void updateDivisionFilterAndVerify(String filter){
        WebElement element = waitForPresenceOfElement(By.xpath(String.format(ddlSelectDivisionFilter, filter)));
        //Work around possible bug where menu is expanded automatically upon page load
        if(!element.isDisplayed()){
            click(lnkDivisionFilter);
        }

        click(By.xpath(String.format(ddlSelectDivisionFilter, filter)));
        click(lnkDivisionFilter);
        verifyFilterApplied(filter);
    }

    public void updateColourFilterAndVerify(String[] updatedColourFilters){
        click(lnkColourFilter);
        for (String filter: updatedColourFilters) {
            click(By.cssSelector(String.format(lnkSelectColourFilter,filter)));
            verifyFilterApplied(filter);
        }
        click(lnkColourFilter);
    }

    public List<HashMap> searchForProductsAndAddToBag(String[] searchQueries, ProductDisplayPage productDisplayPage ) throws ParseException {
        int minResultIndex = 1, maxResultIndex = 6;
        List<HashMap> productsAddedToBagFromSearch = new ArrayList<HashMap>();

        for (int i = 0; i < searchQueries.length; i++) {
            int productToSelectByIndex = NumberUtilities.getRandomNumberInRange(minResultIndex, maxResultIndex);
            openPage(BASE_URL + searchQueries[i]);
            verifyTotalResultsFoundIsGreaterThan(maxResultIndex);
            selectProductFromResults(productToSelectByIndex);
            HashMap<String, String> productSelected = productDisplayPage.selectProductAttributesAndAddToBag();
            productsAddedToBagFromSearch.add(productSelected);
        }
        return productsAddedToBagFromSearch;
    }

    //*********Verifications*********
    public void verifyUpdateFilters(String[] updatedColourFilters, String updatedDivisionFilter, int pricexOffset) {
        updateDivisionFilterAndVerify(updatedDivisionFilter);
        updatePriceFilterAndVerify(pricexOffset);
        updateColourFilterAndVerify(updatedColourFilters);
    }

    public void verifySearchResults(int minimumResultsAmount, int expectedProductImagesCount, String searchQuery) {
        verifyTotalResultsFoundIsGreaterThan(minimumResultsAmount);
        verifyCountInitialProductImagesReturned(expectedProductImagesCount);
        verifySearchQueryPresent(searchQuery);
    }

    private void verifyFilterApplied(String filter) {
        waitForPresenceOfAllElements(By.xpath(String.format(txtFilterApplied,StringUtils.capitalize(filter))));
    }

    public void verifyTotalResultsFoundIsGreaterThan(int minimumResultAmount){
        int actualResultCount = NumberUtilities.extractIntfromString(getTextByAttribute(txtResultCount));
        Assert.assertTrue(actualResultCount >= minimumResultAmount, "FAIL: Found number of results: "
                +actualResultCount + " wasn't >= "+ minimumResultAmount);
    }

    public void verifyCountInitialProductImagesReturned(int expectedProductImagesCount) {
        int actualProductImagesCount = getAllElementsPresent(initialProductsReturned).size();
        Assert.assertEquals(actualProductImagesCount, expectedProductImagesCount
                , "Actual Product Images Count didn't match Expected Product Images Count");
    }

    public void selectProductFromResults(int productIndexNumber){
        WebElement product = getAllElementsPresent(initialProductsReturned).get(productIndexNumber);
        click(product);
    }

    public void verifySearchQueryPresent(String searchQuery){
        String foundQueryText = getTextByAttribute(txtResultQuery);
        Assert.assertEquals(searchQuery, foundQueryText
                , "Actual Search Query text didn't match found Search Query text");
    }

}