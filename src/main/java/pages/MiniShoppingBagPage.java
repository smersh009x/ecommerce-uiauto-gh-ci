package pages;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import util.NumberUtilities;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class MiniShoppingBagPage extends BagPage {

    public MiniShoppingBagPage(WebDriver driver) {
        super(driver);
        txtProductColor = "(//*[@class='mini-cart-attributes']/descendant::*[@class='label'][.='Colour:']/following-sibling::span)[%s]";
        txtProductQuantity = "(//*[@class='mini-cart-attributes']/descendant::*[@class='label'][.='Qty:']/following-sibling::span)[%s]";
        txtProductSize = "(//*[@class='mini-cart-attributes']/descendant::*[@class='label'][.='Size:']/following-sibling::span)[%s]";
        txtProductName = ".mini-cart-products .mini-cart-product:nth-child(%s) .minicart__details--name > a";
        txtProductPrice = ".mini-cart-products .mini-cart-product:nth-child(%s) .mini-cart-list-price";
        productsDisplayedInbag = ".mini-cart-products .mini-cart-product";
    }

    //*********Web Elements*********
    private By lnkViewBag = By.cssSelector(".mini-cart-total a");
    private By txtMiniBagQuantity = By.cssSelector(".mini-cart-total .minicart__quantity");
    private By emptyMiniShoppingBag = By.cssSelector(".mini-cart-total .mini-cart-empty");

    //*********Page Methods*********

    public void clickMiniBag(){
        click(lnkViewBag);
    }

    public void mouseOverMiniBag(){
        mouseOver(lnkViewBag);
    }




    @Override
    protected String getProductName(int i){
        String productName = getTextByAttribute(By.cssSelector(String.format(txtProductName, i)));
        return StringUtils.normalizeSpace(productName);
    }

    @Override
    protected String getProductQuantity(int i){
        String productQuantity  = getTextByAttribute(By.xpath(String.format(txtProductQuantity , i)));
        return StringUtils.normalizeSpace(productQuantity);
    }

    @Override
    protected String getProductColor(int i){
        String productColor = getTextByAttribute(By.xpath(String.format(txtProductColor, i)));
        return StringUtils.normalizeSpace(productColor.toLowerCase());
    }

    @Override
    protected String getProductSize(int i, boolean isTextAbbreviated){
        if (isTextAbbreviated){
            //This is a big hack to work around the mini bag size attribute not working the way the wau the regular bag
            // or product display page work. Ideally we would make the application code more automatoable rather
            //than using hacks of this level. What it's doing is getting the last part of the Full size displayed,
            //e.g. Regular xs, and extracting just the last portion of the string which is the abbreviated size : "xs"
            return StringUtils.normalizeSpace(getTextByAttribute(By.xpath(String.format(txtProductSize, i))))
                    .split("\\s+")[1].toLowerCase();
        }
        return StringUtils.normalizeSpace(getText(By.xpath(String.format(txtProductSize, i))));
    }

    @Override
    protected  String getProductPrice(int i){
            String productPrice = StringUtils.strip(getTextByAttribute(By.cssSelector(String.format(txtProductPrice, i))));
            return productPrice;
    }

    @Override
    public HashMap<String, String> getSelectedProductAttributes(int i) throws ParseException {
        HashMap<String, String> productAttributes = new HashMap<String, String>() {
            {
                put("name", getProductName(i));
                put("color", getProductColor(i));
                put("size", getProductSize(i, true));
                put("price", getProductPrice(i));
                put("quantity", getProductQuantity(i));
            }
        };
        return productAttributes;
    }

    private void updateAttributesOfItemsFromSearch(List productsAddedToBagFromSearch) throws ParseException {
        //update attributes to items added from search to match display of mini Bag
        for (int i = 0; i < productsAddedToBagFromSearch.size(); i++) {
            BigDecimal productQuantity = NumberUtilities.extractPriceFromString( ((HashMap) productsAddedToBagFromSearch.get(i)).get("quantity").toString(), Locale.FRANCE);
            BigDecimal productPrice = NumberUtilities.extractPriceFromString(((HashMap) productsAddedToBagFromSearch.get(i)).get("price").toString(), Locale.FRANCE);
            ((HashMap)productsAddedToBagFromSearch.get(i)).put("price" ,"€"+productPrice.multiply(productQuantity).toString().replace(".",","));
        }
    }

    //*********Verifications*********
    public void verifyMiniShoppingBagIsEmpty(){
        waitForVisibilityOfAllElements(emptyMiniShoppingBag);
    }

    public void verifyMiniBagIconItemCount(List<HashMap> productsAddedToBagFromSearch) {
        int totalQuantity = 0;

        for (HashMap<String, String> product : productsAddedToBagFromSearch){
            totalQuantity += NumberUtilities.extractIntfromString(product.get("quantity"));
        }

        Assert.assertTrue(waitForTextUpdate(txtMiniBagQuantity,String.valueOf(totalQuantity)),
                "The number of items in mini bag was incorrect");
    }

    @Override
    public void verifyProductsInBagMatchAddedProducts(List productsAddedToBagFromSearch) throws ParseException {
        List<HashMap<String, String>> productsInBag = getProductsInBag();
        updateAttributesOfItemsFromSearch(productsAddedToBagFromSearch);

        for (int j = 0; j < productsAddedToBagFromSearch.size(); j++) {
            for (int k = 0; k < productsInBag.size(); k++) {
                if (((HashMap) productsAddedToBagFromSearch.get(j)).containsValue( productsInBag.get(k).get("name") )){
                    Assert.assertEquals(((HashMap) productsAddedToBagFromSearch.get(j)), productsInBag.get(k),
                            "Attributes of product added from Search not matching products in bag: "
                                    + ((HashMap) productsAddedToBagFromSearch.get(j)).toString() +" vs: " +productsInBag.get(k).toString());
                }
            }
        }
    }

}
