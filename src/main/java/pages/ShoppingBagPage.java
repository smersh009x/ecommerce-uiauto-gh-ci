package pages;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import util.LocalDriverManager;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;


public class ShoppingBagPage extends BagPage {

    public ShoppingBagPage(WebDriver driver) {
        super(driver);
        txtProductName = ".cart-display-product:nth-child(%s) .productli__name a";
        txtProductQuantity = ".cart-display-product:nth-child(%s) .cart-quantity-select option[selected='selected']";
        txtProductColor = ".cart-display-product:nth-child(%s) .color a.quickview";
        txtProductSize = ".cart-display-product:nth-child(%s) .size a.quickview";
        productsDisplayedInbag = ".cart-product-details .cart-display-product";
        btnRemoveProduct = ".cart-display-product:nth-child(%s) button[name='removeProduct'] span";
    }

    //*********Web Elements*********
    private String txtProductPrice = ".cart-display-product:nth-child(%s) span[class*='price-standard']";
    private By btnCheckout = By.cssSelector("#checkout-button button[value='Checkout']");

    //*********Page Methods*********
    public void goToShoppingBagPage() {
        LocalDriverManager.getDriver().get(BASE_URL+"/bag");
    }

    public void clickCheckout(){
        click(btnCheckout);
    }

    @Override
    protected String getProductName (int i){
        String productName = getText(By.cssSelector(String.format(txtProductName, i)));
        return productName;
    }

    @Override
    protected String getProductQuantity (int i){
        String productQuantity  = getTextByAttribute(By.cssSelector(String.format(txtProductQuantity , i)));
        return productQuantity ;
    }

    @Override
    protected String getProductColor(int i){
        String productColor = getText(By.cssSelector(String.format(txtProductColor, i)));
        return productColor.toLowerCase();
    }

    @Override
    protected String getProductSize(int i, boolean isTextAbbreviated){
        if (isTextAbbreviated){
            return getText(By.cssSelector(String.format(txtProductSize, i)));
        }
        return waitForPresenceOfElement(By.cssSelector(String.format(txtProductSize, i)))
                .getAttribute("data-size").toLowerCase();
    }

    @Override
    protected  String getProductPrice(int i){
        String productPrice = StringUtils.strip(getText(By.cssSelector(String.format(txtProductPrice, i))));
        return productPrice;
    }

    @Override
    public HashMap<String, String> getSelectedProductAttributes(int i) {
        HashMap<String, String> productAttributes = new HashMap<String, String>() {
            {
                put("name", getProductName(i));
                put("color", getProductColor(i));
                put("size", getProductSize(i, false));
                put("price", getProductPrice(i));
                put("quantity", getProductQuantity(i));
            }
        };
        return productAttributes;
    }

    @Override
    public void verifyProductsInBagMatchAddedProducts(List productsAddedToBagFromSearch) throws ParseException {
        List<HashMap<String, String>> productsInBag = getProductsInBag();

        for (int j = 0; j < productsAddedToBagFromSearch.size(); j++) {
            for (int k = 0; k < productsInBag.size(); k++) {
                if (((HashMap) productsAddedToBagFromSearch.get(j)).containsValue( productsInBag.get(k).get("name") )){
                    Assert.assertEquals(((HashMap) productsAddedToBagFromSearch.get(j)), productsInBag.get(k),
                            "Attributes of product added from Search not matching products in bag: "
                                    + ((HashMap) productsAddedToBagFromSearch.get(j)).toString() +" vs: " +productsInBag.get(k).toString());
                }
            }
        }
    }

}

