package pages;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import util.NumberUtilities;
import util.StringUtilities;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public abstract class BagPage extends BasePage {

    public BagPage(WebDriver driver) {
        super(driver);
    }

    //*********Web Elements*********
    protected String productsDisplayedInbag;
    protected String txtProductName;
    protected String txtProductQuantity;
    protected String txtProductColor;
    protected String txtProductSize;
    protected String  txtProductPrice;
    protected String btnRemoveProduct;

    private String txtRemovedItemMessage = "//div[contains(@class, 'pt_cart--items')]//div[@class='saved-notice'][contains(text(),'%s')]";
    private By txtCartHeaderNotification = By.cssSelector(".cart-items .cart-header__notification");
    private String txtEmptyBagMessage = "Hello, your bag is empty.";
    private By txtOrderSubTotal = By.cssSelector(".order-totals-table .order-subtotal .order-totals-value");
    private By txtOrderShippingValue = By.cssSelector(".order-totals-table .order-shipping .order-totals-value");
    private By txtOrderTotalsValue = By.cssSelector(".order-totals-table .order-total .order-totals-value");


    //*********Page Methods*********

    private int getProductsDisplayedInBagCount() {
        return waitForVisibilityOfAllElements(By.cssSelector(productsDisplayedInbag)).size();
    }

    public void removeProduct(int itemNumber) {
        click(By.cssSelector(String.format(btnRemoveProduct, itemNumber)));
    }

    public List<HashMap<String, String>> getProductsInBag() throws ParseException {
        List<HashMap<String, String>> productsInBag = new ArrayList<>();
        int productsDisplayedInBagCount = getProductsDisplayedInBagCount();

        for (int i = 1; i < productsDisplayedInBagCount + 1; i++) {
            HashMap<String, String> selectedProduct = getSelectedProductAttributes(i);
            productsInBag.add(selectedProduct);
        }
        return productsInBag;
    }


    private String getCartHeaderNotification(){
        String cartHeaderNotification = getText(txtCartHeaderNotification);
        return cartHeaderNotification;
    }

    private String getOrderSubTotalsValue(){
        String orderTotalsValue =getTextByAttribute(txtOrderSubTotal);
        return orderTotalsValue;
    }

    private String getOrderShippingValue(){
        String orderTotalsValue =getTextByAttribute(txtOrderShippingValue);
        return orderTotalsValue;
    }

    private String getOrderTotalsValue(){
        String orderTotalsValue =getTextByAttribute(txtOrderTotalsValue);
        return orderTotalsValue;
    }

    protected abstract String getProductName(int i);

    protected abstract String getProductQuantity (int i);

    protected abstract String getProductColor(int i);

    protected abstract String getProductSize(int i, boolean isTextAbbreviated);

    protected abstract String getProductPrice (int i);

    public abstract HashMap<String, String> getSelectedProductAttributes(int i) throws ParseException;

    //*********Verifications*********
    private void verifyOrderTotals(HashMap<String, String> expectedOrderTotals){
        HashMap<String, String> actualOrderTotals = new HashMap<String, String>() {
            {
                put("subtotal", StringUtils.normalizeSpace(getOrderSubTotalsValue()));
                put("shipping", StringUtils.normalizeSpace(getOrderShippingValue()).toUpperCase());
                put("total", StringUtils.normalizeSpace(getOrderTotalsValue()));
            }
        };
        Assert.assertEquals(actualOrderTotals, expectedOrderTotals);
    }

    private void verifyProductRemovedFromBag(int itemNumber){
        String productName = getProductName(itemNumber);
        String productColor = getProductColor(itemNumber);
        String productSize = getProductSize(itemNumber, true);
        String expectedRemovedItemMessage = String.format("<strong>Removed:</strong>%s,%s,%s",productName, productColor, productSize) ;

        String foundRemovedItemMessage =getTextByAttribute(By.xpath(String.format(txtRemovedItemMessage ,productName)));
        Assert.assertEquals(StringUtilities.removeAllSpaces(expectedRemovedItemMessage).toLowerCase()
                , StringUtilities.removeAllSpaces(foundRemovedItemMessage).toLowerCase()
                ,"Expected removed item from cart message didn't match actual:");
    }

    public void verifyRemoveAllItemsFromBag() throws ParseException {
        List<HashMap<String, String>> productsInBag = getProductsInBag();
        HashMap<String, String> expectedOrderTotals = new HashMap<String, String>() {
            {
                put("subtotal", "€0,00");
                put("shipping", "FREE");
                put("total", "€0,00");
            }
        };

        for (int i = 1; i < productsInBag.size() +1; i++) {
            removeProduct(i);
            verifyProductRemovedFromBag(i);
        }
        verifyOrderTotals(expectedOrderTotals);
        verifyBagHeaderNotification(txtEmptyBagMessage);
    }

    public void verifyProductCountInBag(List productsAddedToBag) throws ParseException {
        Assert.assertEquals(productsAddedToBag.size(), getProductsInBag().size());
    }

    public abstract void verifyProductsInBagMatchAddedProducts(List productsAddedToBagFromSearch) throws ParseException;

    public void verifyBagHeaderNotification(String expectedNotification){
        Assert.assertEquals(expectedNotification, getCartHeaderNotification());
    }



}