package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    //*********Web Elements*********
    private String txtExpectedPageTitle = "My GAP Account Login | Gap® EU";
    private By frmLoginIframe = By.cssSelector("iframe#iFrameResizer0");
    private By txtUsername = By.cssSelector(".ping-row input#username");
    private By txtPassword = By.cssSelector(".ping-row  input#password");
    private By loginButton = By.cssSelector("a.ping-button.allow");
    private By errorMessageText = By.cssSelector("div.ping-error");
    //*********Page Methods*********

    public void enterUsername(String username){
        switchToIframe(frmLoginIframe);
        clearAndTypeText(txtUsername,username);
        switchOutOfIframe();
    }

    public void enterPassword(String password){
        switchToIframe(frmLoginIframe);
        clearAndTypeText(txtPassword,password);
        switchOutOfIframe();
    }

    public void clickLogin(){
        switchToIframe(frmLoginIframe);
        click(loginButton);
    }

    public String getExpectedPageTitle(){
        return txtExpectedPageTitle;
    }

    public void loginUser(String userName, String password, TopNav topNav) {
        topNav.openUserAccountMenu();
        topNav.clickSignInUser();
        enterUsername(userName);
        enterPassword(password);
        clickLogin();
    }

    //*********Verifications*********
    public void verifyLoginErrorText(String expectedText) {
        String actualtext = getTextByAttribute( errorMessageText);
        Assert.assertEquals(actualtext, expectedText);
    }
}