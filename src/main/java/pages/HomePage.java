package pages;

import org.openqa.selenium.WebDriver;


public class HomePage extends BasePage {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    //*********Web Elements*********

    //*********Page Methods*********
    public void goToHomepage() {
        driver.get(BASE_URL);
    }

    public void goToHomePageAndDismissPopups(Popups popups) {
        goToHomepage();
        popups.dismissAllVisiblePopups();
    }
}