package util;

import java.util.UUID;

public class StringUtilities {

    public static String removeAllSpaces(String text){
        return text.replaceAll("\\s","");
    }

    public static String generateUniqueId(){
        String randomString = UUID.randomUUID().toString();
        return randomString.substring(0,randomString.length()-8);
    }
}
